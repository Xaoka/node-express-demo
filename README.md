# Repo

A demo of NodeJS + Express as a backend API provider.

## Problem Space

Big Books Publishing Co. is developing a tool to track the state of different
projects that their editors are managing. From the moment they receive a
manuscript to the point where the book is in print, the project goes
through multiple states and involves multiple roles:

- Their pipeline consists of the following stages: Received, Reviewed, Accepted, Contract Signed, Edited, Copyedited, Typeset, In Print.
- For each project, they need to store:
- Working title
- Authors
- Stage
- Main editor
- Person currently processing it
- Creation date
- Update date

## Technical Requirements

Using Node and Express, build an REST API that can create a new project, update an existing project, and list all projects. All decisions about parameters, database and internal structure will be up to you. As an optional challenge, you can create an endpoint that for a specific project returns a timeline of the different stages the project has been on, with timestamps.
