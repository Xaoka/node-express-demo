
module.exports =
{
    validateFields: function (requiredFields, userData)
    {
        for (const field of requiredFields)
        {
            if (userData[field] === undefined || userData[field] === null)
            {
                throw Error(`Required field "${field}" not provided.`)
            }
        }
    }
}