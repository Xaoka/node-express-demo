var express = require('express');
var router = express.Router();
  
// Load the AWS SDK for Node.js
var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({region: 'eu-west-2'});
var credentials = new AWS.SharedIniFileCredentials({profile: 'big-books'});
AWS.config.credentials = credentials;

// Create the DynamoDB service object
var ddb = new AWS.DynamoDB({apiVersion: '2012-08-10'});

var validation = require('../utils/validation');

// List all
router.get('/projects', function(req, res, next) {
  
  ddb.scan({ TableName: 'projects' }, (err, data) =>
  {
    if (err) {
      res.send({ "msg": "Error", "data": data});
      return;
    }

    // Does AWS provide an extraction method?
    data.Items.forEach((item) =>
    {
      for (const key of Object.keys(item))
      {
        item[key] = item[key].S;
      }
    })
    res.send({ "msg": "Success", "data": data.Items});
  })
});

const validStages = ["Received", "Reviewed", "Accepted", "Contract Signed", "Edited", "Copyedited", "Typeset", "In Print"];
function isValidStage(stage)
{
  if (stage)
  {
    if (!validStages.includes(stage))
    {
      return false;
    }
  }
  return true;
}

// CREATE
router.post('/projects', function(req, res, next) {
  
  const userData = req.body;
  const requiredFields = [ "author", "title" ];
  const allowedFields = [ "author", "title", "processor", "stage", "editor" ];
  try
  {
    validation.validateFields(requiredFields, userData);
  }
  catch (error)
  {
    res.send(JSON.stringify({ msg: "Error", data: error }))
    return;
  }
  if (!isValidStage(userData.stage))
  { 
    res.send(JSON.stringify({ msg: "Error", data: `${userData.stage} is not a valid stage. Only ${validStages.join(`, `)} are allowed` }))
    return;
  }

  // Convert user data into AWS format
  const postData = {};
  for (const field of allowedFields)
  {
    if (userData[field])
    {
      postData[field] = { S: userData[field].toString() }
    }
  }

  // Timestamp
  const date = (new Date()).toLocaleDateString();
  postData.created_on = { S: date };
  postData.updated_on = { S: date };
  
  var params = {
    TableName: 'projects',
    Item: postData
  };

  // Call DynamoDB to add the item to the table
  ddb.putItem(params, defaultResponse(res));
});

// UPDATE
router.put('/projects', function(req, res, next) {
  
  const userData = req.body;
  const requiredFields = [ "author", "title" ];
  const allowedFields = [ "author", "title", "processor", "stage", "editor" ];
  try
  {
    validation.validateFields(requiredFields, userData);
  }
  catch (error)
  {
    res.send(JSON.stringify({ msg: "Error", data: `Validation Error: ${error}` }))
    return;
  }
  if (!isValidStage(userData.stage))
  { 
    res.send(JSON.stringify({ msg: "Error", data: `${userData.stage} is not a valid stage. Only ${validStages.join(`, `)} are allowed` }))
    return;
  }

  // Convert user data into AWS format
  const postData = {};
  for (const field of allowedFields)
  {
    if (userData[field])
    {
      postData[field] = { S: userData[field].toString() }
    }
  }

  // Timestamp
  const date = (new Date()).toLocaleDateString();
  postData.created_on = { S: date };
  postData.updated_on = { S: date };
  
  var params = {
    TableName: 'projects',
    // Item: postData
    Key: { 'author': postData.author },
    UpdateExpression: `set title = :t, processor = :p, stage = :s, editor = :e`,
    ExpressionAttributeValues:{
        ":t": postData.title,
        ":p": postData.processor,
        ":s": postData.stage,
        ":e": postData.editor
    },
  };

  // Call DynamoDB to add the item to the table
  ddb.updateItem(params, defaultResponse(res));
});

function defaultResponse(res)
{
  return function (err, data) {
    if (err) {
      console.log("Error", err);
      res.send({ "msg": "Error", "data": `DefaultError: ${data}`});
    } else {
      console.log("Success", data);
      res.send({ "msg": "Success", "data": data});
    }
  }
};

module.exports = router;
